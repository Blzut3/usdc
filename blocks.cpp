// Copyright (c) 2010, Braden "Blzut3" Obrzut <admin@maniacsvault.net>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <cstring>

#include "blocks.h"

inline static uint32_t READINT32(const char* pointer)
{
	return ((uint32_t(uint8_t(pointer[0]))) |
		(uint32_t(uint8_t(pointer[1]))<<8) |
		(uint32_t(uint8_t(pointer[2]))<<16) |
		(uint32_t(uint8_t(pointer[3]))<<24));
}
inline static void WRITEINT32(char* pointer, uint32_t integer)
{
	pointer[0] = (uint8_t)(integer&0xFF);
	pointer[1] = (uint8_t)((integer>>8)&0xFF);
	pointer[2] = (uint8_t)((integer>>16)&0xFF);
	pointer[3] = (uint8_t)((integer>>24)&0xFF);
}

Response::Response() : defined(false), giveID(-1), link(0), log(0)
{
	costID[0] = costID[1] = costID[2] = 0;
	costAmount[0] = costAmount[1] = costAmount[2] = 0;
	memset(response, 0, 32);
	memset(yesMessage, 0, 80);
	memset(noMessage, 0, 80);
}

void Response::Read(const char* buffer)
{
	giveID = READINT32(&buffer[0]);
	costID[0] = READINT32(&buffer[4]);
	costID[1] = READINT32(&buffer[8]);
	costID[2] = READINT32(&buffer[12]);
	costAmount[0] = READINT32(&buffer[16]);
	costAmount[1] = READINT32(&buffer[20]);
	costAmount[2] = READINT32(&buffer[24]);
	memcpy(response, buffer + 28, 33);
	memcpy(yesMessage, buffer + 60, 81);
	link = READINT32(&buffer[140]);
	log = READINT32(&buffer[144]);
	memcpy(noMessage, buffer + 148, 81);
}

void Response::Save(std::ostream &out) const
{
	char outBuffer[228];
	// Vanilla strife uses a giveID == 0 to determine the end of the responses list.
	WRITEINT32(&outBuffer[0], defined ? giveID : 0);
	WRITEINT32(&outBuffer[4], costID[0]);
	WRITEINT32(&outBuffer[8], costID[1]);
	WRITEINT32(&outBuffer[12], costID[2]);
	WRITEINT32(&outBuffer[16], costAmount[0]);
	WRITEINT32(&outBuffer[20], costAmount[1]);
	WRITEINT32(&outBuffer[24], costAmount[2]);
	memcpy(outBuffer + 28, response, 32);
	memcpy(outBuffer + 60, yesMessage, 80);
	WRITEINT32(&outBuffer[140], link);
	WRITEINT32(&outBuffer[144], log);
	memcpy(outBuffer + 148, noMessage, 80);
	out.write(outBuffer, 228);
}

Speech::Speech() : speakerID(0), dropID(0), link(0), voiceNumber(0)
{
	checkID[0] = checkID[1] = checkID[2] = 0;
	memset(name, 0, 17);
	memset(sound, 0, 9);
	memset(backdrop, 0, 9);
	memset(dialog, 0, 321);
}

void Speech::Read(const char* buffer, bool oldFormat)
{
	speakerID = READINT32(&buffer[0]);
	dropID = READINT32(&buffer[4]);
	if(!oldFormat)
	{
		checkID[0] = READINT32(&buffer[8]);
		checkID[1] = READINT32(&buffer[12]);
		checkID[2] = READINT32(&buffer[16]);
		link = READINT32(&buffer[20]);
		memcpy(name, buffer + 24, 16);
		memcpy(sound, buffer + 40, 8);
		memcpy(backdrop, buffer + 48, 8);
		memcpy(dialog, buffer + 56, 320);
	}
	else
	{
		voiceNumber = READINT32(&buffer[8]);
		memcpy(name, buffer + 12, 16);
		memcpy(dialog, buffer + 28, 320);
	}

	for(unsigned int i = 0;i < 5;i++)
		responses[i].Read(buffer + (oldFormat ? 348 : 376) + i*228);
}

void Speech::Save(std::ostream &out, bool oldFormat) const
{
	char outBuffer[376];
	WRITEINT32(&outBuffer[0], speakerID);
	WRITEINT32(&outBuffer[4], dropID);
	if(!oldFormat)
	{
		WRITEINT32(&outBuffer[8], checkID[0]);
		WRITEINT32(&outBuffer[12], checkID[1]);
		WRITEINT32(&outBuffer[16], checkID[2]);
		WRITEINT32(&outBuffer[20], link);
		memcpy(outBuffer + 24, name, 16);
		memcpy(outBuffer + 40, sound, 8);
		memcpy(outBuffer + 48, backdrop, 8);
		memcpy(outBuffer + 56, dialog, 320);
	}
	else
	{
		WRITEINT32(&outBuffer[8], voiceNumber);
		memcpy(outBuffer + 12, name, 16);
		memcpy(outBuffer + 28, dialog, 320);
	}
	out.write(outBuffer, oldFormat ? 348 : 376);

	for(unsigned int i = 0;i < 5;i++)
		responses[i].Save(out);
}
