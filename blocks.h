// Copyright (c) 2010, Braden "Blzut3" Obrzut <admin@maniacsvault.net>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef __BLOCKS_H__
#define __BLOCKS_H__

#include <ostream>
#include <stdint.h>

class Response
{
	public:
		Response();

		bool		defined;

		int32_t		giveID;
		int32_t		costID[3];
		int32_t		costAmount[3];
		char		response[33];
		char		yesMessage[81];
		int32_t		link;
		uint32_t	log;
		char		noMessage[81];

		void		Read(const char* buffer);
		void		Save(std::ostream &out) const;
};

class Speech
{
	public:
		Speech();

		uint32_t	speakerID;
		int32_t		dropID;
		int32_t		checkID[3];
		int32_t		link;
		char		name[17];
		char		sound[9];
		char		backdrop[9];
		char		dialog[321];
		Response	responses[5];

		// Old format
		uint32_t	voiceNumber;

		void		Read(const char* buffer, bool oldFormat);
		void		Save(std::ostream &out, bool oldFormat) const;
};

#endif /* __BLOCKS_H__ */
