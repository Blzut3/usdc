# Sphinx configuration.  This is only used for generating the man page.  If you
# don't care about the man page then don't bother installing sphinx.

project = 'USDC'
copyright = '2010-2019, Braden "Blzut3" Obrzut'
author = 'Braden "Blzut3" Obrzut'

version = '1.7'
release = '1.7'

extensions = []
templates_path = []
source_suffix = '.rst'

master_doc = 'README'

language = None

exclude_patterns = []

pygments_style = 'sphinx'

man_pages = [
	('README', 'usdc', 'Universal Strife Dialog Compiler', [author], 1)
]
