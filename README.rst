usdc(1)
======= 

Synopsis
--------

**usdc** [options] file

Description
-----------

**usdc** is a best effort compiler for Universal Strife Dialog Format
scripts. The output of the compiler are binary lumps which can be used by the
vanilla Stife game engine.

The compilation is described as best effort since the USDF script format is
more flexible than the original binary format permits. When a limit is exceeded
the compiler will produce a warning diagnostic.

Options
-------

.. program:: usdc

.. option:: -c

	Set mode to compile (default).

.. option:: -d

	Set mode to decompile.

.. option:: -k

	Don't discard the first cost block if amount is zero and a mobj is
	specified.  Normally it's optimized out on decompile to reduce noise, but
	if you are seeking a byte-for-byte recompile then use this option.

.. option:: -o file

	Sets the name of the output file.

.. option:: -r

	Remove blank yes messages on decompile like ZDoom.

.. option:: -t

	Compile in the old teaser format. Not recommended.

Scripts
-------

This program compiles scripts in a format agreed upon by multiple parties known
as Universal Strife Dialog Format.  Some USDF parameters are not supported due 
to format limitations, but if provided they will be safely ignored by the 
compiler (with just a warning printed).  Below is the outline from the USDF 2.2
spec adjusted for this program. ::

	conversation // Starts a dialog.
	{
	    actor = <integer>; // mobj for this conversation's actor.  If
	                       // previously used, this will override the
	                       // previous conversation.
	
	    page // Starts a new page.  Pages are automatically numbered
	         // starting at 1.
	    {
	        name   = <string>;  // Name that goes in the upper left hand
	                            // corner.
	        panel  = <string>;  // Name of lump to render as the
	                            // background.
	        voice  = <string>;  // Narration sound lump.
	        dialog = <rstring>; // Dialog of the page.
	        drop   = <integer>; // mobj for the object to drop if the
	                            // actor is  killed.
	        link   = <integer>; // Page to jump to if all ifitem
	                            // conditions are satisified.
	
	        voicenumber = <integer>; // Voice number for old format
	                                 // dialogs.
	
	        // jumps to the specified page if the player has the specified
	        // amount or more of item in their inventory. This can be
	        // repeated as manytimes as the author wants, all conditions
	        // must be met for the jump to occur.
	        ifitem
	        {
	            item   = <integer>; // mobj of item to check.
	            amount = <integer>; // amount required to be in inventory.
	        }
	
	        // Choices shall be automatically numbered.  (Up to 5)
	        choice
	        {
	            text            = <string>;  // Name of the choice.
	
	            // The amount of an item needed to successfully pick this
	            // option.  This can be repeated, but only the first will
	            // be shown (provided  displaycost is true).  All costs
	            // must be satisfied for success.
	            cost
	            {
	                item   = <integer>; // Item that is required for this
	                                    // option.
	                amount = <integer>; // Minimum amount of the item
	                                    // needed.
	            }
	
	            displaycost     = <bool>;    // Weather the cost should be
	                                         // displayed with the option.
	                                         // Default: false
	            yesmessage      = <string>;  // Text to add to console
	                                         // when choice is accepted.
	            nomessage       = <string>;  // Text to add to console
	                                         // when choice is denied.
	
	            log             = <string>;  // LOG entry to use on
	                                         // success.  For the purposes
	                                         // of this program you must
	                                         / use "LOGXXXXX".
	            giveitem        = <integer>; // Gives the specified item
	                                         // upon success.
	
	            nextpage        = <integer>; // Sets the next page.
	            closedialog     = <bool>;    // Should the dialog be
	                                         // closed upon selecting this
	                                         // choice?
	                                         // Default: false
	        }
	    }
	}

This compiler supports compiling to the old teaser dialog format, however the 
use of this format is not recommended and is more limited.  A voicenumber 
property is provided for this format, but should not be used otherwise.

In addition to be warning free (and thus USDF compliant) you must start your 
script with the following::

	namespace = "Strife";
	include = "SCRIPT00";

If you are building a new SCRIPT00 it's best you leave out the include line and
ingnore the warning, however it doesn't actually affect the compiler (besides 
removing the warning).
