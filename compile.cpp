// Copyright (c) 2010, Braden "Blzut3" Obrzut <admin@maniacsvault.net>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <cstring>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <list>
using namespace std;

#include "blocks.h"
#include "compile.h"
#include "main.h"
#include "scanner.h"

void IgnoreKey(Scanner &sc)
{
	Warning("%d:%d:Ignoring unknown key \"%s\".", sc.GetLine(), sc.GetLinePos(), sc.str.c_str());
	if(sc.CheckToken('{'))
	{
		int level = 1;
		while(sc.TokensLeft())
		{
			if(sc.CheckToken('}'))
			{
				level--;
				if(level == 0)
					break;
			}
			else if(sc.CheckToken('{'))
				level++;
			else
				sc.GetNextToken();
		}
	}
	else
	{
		sc.MustGetToken('=');
		do
		{
			sc.GetNextToken();
		}
		while(sc.token != ';');
	}
}

bool GetKey(Scanner &sc, char expectedToken)
{
	if(sc.CheckToken('='))
	{
		bool negate = false;
		if(expectedToken == TK_IntConst && sc.CheckToken('-'))
			negate = true;

		if(sc.CheckToken(expectedToken))
		{
			if(negate)
				sc.number *= -1;
			return true;
		}

		sc.GetNextToken();
		if(sc.token < TK_NumSpecialTokens)
			Warning("%d:%d:Key has bad value \"%s\", ignoring.", sc.GetLine(), sc.GetLinePos(), sc.TokenNames[sc.token]);
		else
			Warning("%d:%d:Key has bad value \"%c\", ignoring.", sc.GetLine(), sc.GetLinePos(), sc.token);
		do
		{
			sc.GetNextToken();
		}
		while(sc.token != ';');
	}
	else
		IgnoreKey(sc);
	return false;
}

bool ParseCost(Scanner &sc, Response &response, int index)
{
	if(sc.CheckToken('{'))
	{
		while(!sc.CheckToken('}'))
		{
			sc.MustGetToken(TK_Identifier);
			if(sc.str.compare("item") == 0)
			{
				if(GetKey(sc, TK_IntConst))
				{
					response.costID[index] = sc.number;
					sc.MustGetToken(';');
				}
			}
			else if(sc.str.compare("amount") == 0)
			{
				if(GetKey(sc, TK_IntConst))
				{
					response.costAmount[index] = sc.number;
					sc.MustGetToken(';');
				}
			}
			else
				IgnoreKey(sc);
		}
	}
	else
	{
		Warning("%d:%d:Cost not a sub-block as expected.", sc.GetLine(), sc.GetLinePos());
		IgnoreKey(sc);
	}
	return false;
}

bool ParseChoice(Scanner &sc, Response &response)
{
	response.defined = true;

	if(sc.CheckToken('{'))
	{
		unsigned int costs = 0;
		bool displayCost = false;
		bool closeDialog = false;

		while(!sc.CheckToken('}'))
		{
			sc.MustGetToken(TK_Identifier);
			if(sc.str.compare("text") == 0)
			{
				if(GetKey(sc, TK_StringConst))
				{
					strncpy(response.response, sc.str.c_str(), 32);
					if(sc.str.length() > 32)
						Warning("%d:%d:Text too long, truncating to 32 characters.", sc.GetLine(), sc.GetLinePos());
					sc.MustGetToken(';');
				}
			}
			else if(sc.str.compare("cost") == 0)
			{
				if(costs > 2)
					Warning("%d:%d:Too many cost blocks, ignoring.", sc.GetLine(), sc.GetLinePos());
				else
				{
					ParseCost(sc, response, costs);
					costs++;
				}
			}
			else if(sc.str.compare("displaycost") == 0)
			{
				if(GetKey(sc, TK_BoolConst))
				{
					displayCost = sc.boolean;
					sc.MustGetToken(';');
				}
			}
			else if(sc.str.compare("yesmessage") == 0)
			{
				if(GetKey(sc, TK_StringConst))
				{
					strncpy(response.yesMessage, sc.str.c_str(), 80);
					if(sc.str.length() > 80)
						Warning("%d:%d:Yesmessage too long, truncating to 80 characters.", sc.GetLine(), sc.GetLinePos());
					sc.MustGetToken(';');
				}
			}
			else if(sc.str.compare("nomessage") == 0)
			{
				if(GetKey(sc, TK_StringConst))
				{
					strncpy(response.noMessage, sc.str.c_str(), 80);
					if(sc.str.length() > 80)
						Warning("%d:%d:Nomessage too long, truncating to 80 characters.", sc.GetLine(), sc.GetLinePos());
					sc.MustGetToken(';');
				}
			}
			else if(sc.str.compare("log") == 0)
			{
				if(GetKey(sc, TK_StringConst))
				{
					if(sc.str.length() < 4 || strncmp(sc.str.c_str(), "LOG", 3) != 0)
						Warning("%d:%d:Log must be in the format of LOG# to compile, ignoring.\n", sc.GetLine(), sc.GetLinePos());
					else
					{
						int num = atoi(sc.str.c_str() + 3);
						response.log = num;
						sc.MustGetToken(';');
					}
				}
			}
			else if(sc.str.compare("giveitem") == 0)
			{
				if(GetKey(sc, TK_IntConst))
				{
					response.giveID = sc.number;
					sc.MustGetToken(';');
				}
			}
			else if(sc.str.compare("nextpage") == 0)
			{
				if(GetKey(sc, TK_IntConst))
				{
					response.link = sc.number;
					sc.MustGetToken(';');
				}
			}
			else if(sc.str.compare("closedialog") == 0)
			{
				if(GetKey(sc, TK_BoolConst))
				{
					closeDialog = sc.boolean;
					sc.MustGetToken(';');
				}
			}
			else
				IgnoreKey(sc);
		}

		if(!displayCost)
		{
			if(costs > 2 && response.costAmount[0] > 0)
			{
				Warning("%d:%d:Choice can't have displaycost off and use three cost blocks at the same time in vanilla strife.", sc.GetLine(), sc.GetLinePos());
				response.costAmount[0] *= -1;
			}
			else if(response.costAmount[0] != 0)
			{
				// Shift cost blocks one position and wipe out first block's data.
				response.costAmount[2] = response.costAmount[1];
				response.costAmount[1] = response.costAmount[0];
				response.costAmount[0] = 0;
				response.costID[2] = response.costID[1];
				response.costID[1] = response.costID[0];
				// This just allows us to produce something closer to what the
				// original tools produced, but it's not 100%. Some dialogs
				// with no costs still had this set as A8, some do not.
				response.costID[0] = costs >= 1 ? 0xA8 : 0;
			}
		}
		if(!closeDialog)
			response.link *= -1;
	}
	else
	{
		Warning("%d:%d:Choice not a sub-block as expected.", sc.GetLine(), sc.GetLinePos());
		IgnoreKey(sc);
	}
	return false;
};

bool ParseIfItem(Scanner &sc, Speech &speech, int index)
{
	if(sc.CheckToken('{'))
	{
		while(!sc.CheckToken('}'))
		{
			sc.MustGetToken(TK_Identifier);
			if(sc.str.compare("item") == 0)
			{
				if(GetKey(sc, TK_IntConst))
				{
					speech.checkID[index] = sc.number;
					sc.MustGetToken(';');
				}
			}
			else if(sc.str.compare("amount") == 0)
			{
				if(GetKey(sc, TK_IntConst))
				{
					if(sc.number != 1)
						Warning("%d:%d:Binary format does not support ifitem amounts other than 1.", sc.GetLine(), sc.GetLinePos());
					sc.MustGetToken(';');
				}
			}
			else if(sc.str.compare("page") == 0)
			{
				if(GetKey(sc, TK_IntConst))
				{
					if(index == 0)
						speech.link = sc.number;
					else if(signed(sc.number) != speech.link)
						Warning("%d:%d:Ignoring differing ifitem jump.", sc.GetLine(), sc.GetLinePos());
					sc.MustGetToken(';');
				}
			}
			else
				IgnoreKey(sc);
		}
	}
	else
	{
		Warning("%d:%d:Ifitem not a sub-block as expected.", sc.GetLine(), sc.GetLinePos());
		IgnoreKey(sc);
	}
	return false;
}

bool ParsePage(Scanner &sc, Speech &speech, bool oldformat)
{
	if(sc.CheckToken('{'))
	{
		unsigned int items = 0;
		unsigned int choices = 0;

		while(!sc.CheckToken('}'))
		{
			sc.MustGetToken(TK_Identifier);
			if(sc.str.compare("name") == 0)
			{
				if(GetKey(sc, TK_StringConst))
				{
					strncpy(speech.name, sc.str.c_str(), 16);
					if(sc.str.length() > 16)
						Warning("%d:%d:Character name too long, truncating to 16 characters.", sc.GetLine(), sc.GetLinePos());
					sc.MustGetToken(';');
				}
			}
			else if(sc.str.compare("panel") == 0)
			{
				if(GetKey(sc, TK_StringConst))
				{
					strncpy(speech.backdrop, sc.str.c_str(), 8);
					if(sc.str.length() > 8)
						Warning("%d:%d:Panel name too long, truncating to 8 characters.", sc.GetLine(), sc.GetLinePos());
					sc.MustGetToken(';');
				}
			}
			else if(sc.str.compare("voice") == 0)
			{
				if(oldformat)
				{
					Warning("%d:%d:Voice not applicable to old format scripts, use voicenumber instead.", sc.GetLine(), sc.GetLinePos());
					IgnoreKey(sc);
				}
				else
				{
					if(GetKey(sc, TK_StringConst))
					{
						strncpy(speech.sound, sc.str.c_str(), 8);
						if(sc.str.length() > 8)
							Warning("%d:%d:Voice name too long, truncating to 8 characters.", sc.GetLine(), sc.GetLinePos());
						sc.MustGetToken(';');
					}
				}
			}
			else if(sc.str.compare("voicenumber") == 0)
			{
				if(!oldformat)
				{
					Warning("%d:%d:Voicenumber only applicable to old format scripts, use voice instead.", sc.GetLine(), sc.GetLinePos());
					IgnoreKey(sc);
				}
				else
				{
					if(GetKey(sc, TK_IntConst))
					{
						speech.voiceNumber = sc.number;
						sc.MustGetToken(';');
					}
				}
			}
			else if(sc.str.compare("dialog") == 0)
			{
				if(GetKey(sc, TK_StringConst))
				{
					strncpy(speech.dialog, sc.str.c_str(), 320);
					if(sc.str.length() > 320)
						Warning("%d:%d:Dialog too long, truncating to 320 characters. (Honestly, did you really think this string is going to fit on screen anyways?)", sc.GetLine(), sc.GetLinePos());
					sc.MustGetToken(';');
				}
			}
			else if(sc.str.compare("drop") == 0)
			{
				if(GetKey(sc, TK_IntConst))
				{
					speech.dropID = sc.number;
					sc.MustGetToken(';');
				}
			}
			else if(sc.str.compare("ifitem") == 0)
			{
				if(items > 2)
					Warning("%d:%d:Too many ifitem blocks, ignoring.", sc.GetLine(), sc.GetLinePos());
				else
				{
					ParseIfItem(sc, speech, items);
					items++;
				}
			}
			else if(sc.str.compare("link") == 0)
			{
				if(GetKey(sc, TK_IntConst))
				{
					speech.link = sc.number;
					sc.MustGetToken(';');
				}
			}
			else if(sc.str.compare("choice") == 0)
			{
				if(choices > 4)
					Warning("%d:%d:Too many choices blocks, ignoring.", sc.GetLine(), sc.GetLinePos());
				else
				{
					ParseChoice(sc, speech.responses[choices]);
					choices++;
				}
			}
			else
				IgnoreKey(sc);
		}
		return true;
	}
	else
	{
		Warning("%d:%d:Page not a sub-block as expected.", sc.GetLine(), sc.GetLinePos());
		IgnoreKey(sc);
	}
	return false;
}

bool ParseConversation(Scanner &sc, list<Speech> &script, bool oldformat)
{
	if(sc.CheckToken('{'))
	{
		list<Speech> newScripts;

		int actorID = -1;
		while(!sc.CheckToken('}'))
		{
			sc.MustGetToken(TK_Identifier);
			if(sc.str.compare("actor") == 0)
			{
				if(GetKey(sc, TK_IntConst))
				{
					actorID = sc.number;
					sc.MustGetToken(';');
				}
			}
			else if(sc.str.compare("page") == 0)
			{
				Speech newSpeech;
				if(ParsePage(sc, newSpeech, oldformat))
				{
					newScripts.push_back(newSpeech);
				}
			}
			else
				IgnoreKey(sc);
		}

		if(actorID == -1)
		{
			Error("%d:%d:A conversation block must contain an actor property.", sc.GetLine(), sc.GetLinePos());
		}
		else
		{
			for(list<Speech>::iterator iter = newScripts.begin();iter != newScripts.end();iter++)
			{
				(*iter).speakerID = actorID;
				script.push_back(*iter);
			}
		}
		return true;
	}
	else
	{
		Warning("%d:%d:Conversation not a sub-block as expected.", sc.GetLine(), sc.GetLinePos());
		IgnoreKey(sc);
	}
	return false;
}

void Compile(const char* buffer, int size, std::ostream &out, bool oldformat)
{
	Scanner::SetErrorCallback(Error);

	Scanner sc(buffer, size);

	list<Speech> script;

	bool firstProperty = true;
	bool script00Included = false;
	while(sc.TokensLeft())
	{
		sc.MustGetToken(TK_Identifier);
		if(sc.str.compare("conversation") == 0)
		{
			ParseConversation(sc, script, oldformat);
		}
		else if(sc.str.compare("namespace") == 0)
		{
			if(!(GetKey(sc, TK_StringConst) && sc.str.compare("Strife") == 0))
				Warning("%d:%d:This compiler only understands the \"Strife\" namespace.", sc.GetLine(), sc.GetLinePos());
			if(!firstProperty)
				Warning("%d:%d:USDF spec requires that namespace be declared first and should only be declared once.", sc.GetLine(), sc.GetLinePos());
			firstProperty = false;
			sc.MustGetToken(';');
		}
		else if(sc.str.compare("include") == 0)
		{
			if(GetKey(sc, TK_StringConst))
			{
				if(sc.str.compare("SCRIPT00") == 0)
					script00Included = true;
			}
			sc.MustGetToken(';');
		}
		else
		{
			IgnoreKey(sc);
		}

		if(firstProperty) // namespace sets firstProperty to false in order to allow us to simplify this warning.
		{
			Warning("USDF spec requires that `namespace = \"Strife\";` be the first command in a USDF document.");
			firstProperty = false;
		}
	}

	if(!script00Included)
		Warning("\"SCRIPT00\" not included explicitly.  Ignore this warning if compiling SCRIPT00.");

	for(list<Speech>::iterator iter = script.begin();iter != script.end();iter++)
	{
		(*iter).Save(out, oldformat);
	}
}

void Decompile(const char* buffer, int size, std::ostream &out, bool oldformat, bool removeblanks, bool keepcostitems)
{
	bool sourceIsOld = false;
	if(size % 1488 == 0 && (size % 1516 != 0 || oldformat))
	{
		Warning("Decompiling teaser scripts will result in non-standard USDF.");
		sourceIsOld = true;
	}
	else if(size % 1516 != 0)
		Error("Compiled file unrecognizable as a Strife dialog.");

	list<Speech> script;
	for(int i = 0;i < size;i += (sourceIsOld ? 1488 : 1516))
	{
		Speech next;
		next.Read(buffer + i, sourceIsOld);
		script.push_back(next);
	}

	int lastID = -1;
	out << "// Decompiled by Universal Strife Dialog Compiler.\n\nnamespace = \"Strife\";\ninclude = \"SCRIPT00\";\n\n";
	for(list<Speech>::iterator iter=script.begin();iter != script.end();iter++)
	{
		Speech &speech = *iter;
		if(unsigned(lastID) != speech.speakerID)
		{
			if(lastID != -1)
				out << "}\n\n";
			out << "conversation\n{\n\tactor = " << speech.speakerID << ";\n";
			lastID = speech.speakerID;
		}

		// Pages
		out << "\tpage\n\t{\n";
		if(speech.name[0] != 0)
			out << "\t\tname = \"" << Scanner::Escape(speech.name) << "\";\n";
		if(speech.backdrop[0] != 0)
			out << "\t\tpanel = \"" << Scanner::Escape(speech.backdrop) << "\";\n";
		if(speech.sound[0] != 0)
			out << "\t\tvoice = \"" << Scanner::Escape(speech.sound) << "\";\n";
		if(speech.voiceNumber != 0)
			out << "\t\tvoiceNumber = " << speech.voiceNumber << ";\n";
		if(speech.dialog[0] != 0)
			out << "\t\tdialog = \"" << Scanner::Escape(speech.dialog) << "\";\n";
		if(speech.dropID > 0)
			out << "\t\tdrop = " << speech.dropID << ";\n";
		if(speech.link > 0)
			out << "\t\tlink = " << speech.link << ";\n";
		int lastUsedCheck = 0;
		for(int i = 0;i < 3;i++)
		{
			if(speech.checkID[i] > 0)
				lastUsedCheck = i+1;
		}
		for(int i = 0;i < lastUsedCheck;i++)
		{
			out << "\t\tifitem\n\t\t{\n\t\t\titem = " << speech.checkID[i] << ";\n\t\t\tamount = 1;\n\t\t}\n";
		}
		for(int i = 0;i < 5;i++)
		{
			if(speech.responses[i].response[0] != 0)
			{
				out << "\t\tchoice\n\t\t{\n\t\t\ttext = \"" << Scanner::Escape(speech.responses[i].response) << "\";\n";
				// Due to the way the costs work, if we are not displaying a
				// cost the first cost block is eaten.
				bool displayCost = speech.responses[i].costAmount[0] > 0;
				bool writeFirstCost = displayCost || (keepcostitems && speech.responses[i].costID[0] > 0);
				int lastUsedCost = 0;
				for(int j = 0;j < 3;j++)
				{
					// In vanilla 0 is used for none.
					if(speech.responses[i].costID[j] > 0)
						lastUsedCost = j+1;
				}
				for(int j = writeFirstCost ? 0 : 1;j < lastUsedCost;j++)
				{
					out << "\t\t\tcost\n\t\t\t{\n\t\t\t\titem = " << speech.responses[i].costID[j] << ";\n\t\t\t\tamount = " << static_cast<int> (abs(static_cast<double> (speech.responses[i].costAmount[j]))) << ";\n\t\t\t}\n";
				}
				if(speech.responses[i].costID[0] > 0 && lastUsedCost != (displayCost ? 0 : 1))
					out << "\t\t\tdisplaycost = " << (displayCost ? "true" : "false") << ";\n";
				if(speech.responses[i].yesMessage[0] != 0)
				{
					if(!removeblanks || strcmp(speech.responses[i].yesMessage, "_") != 0)
						out << "\t\t\tyesmessage = \"" << Scanner::Escape(speech.responses[i].yesMessage) << "\";\n";
				}
				if(speech.responses[i].noMessage[0] != 0)
					out << "\t\t\tnomessage = \"" << Scanner::Escape(speech.responses[i].noMessage) << "\";\n";
				if(speech.responses[i].log != 0)
					out << "\t\t\tlog = \"LOG" << speech.responses[i].log << "\";\n";
				if(speech.responses[i].giveID >= 0)
					out << "\t\t\tgiveitem = " << speech.responses[i].giveID << ";\n";
				if(speech.responses[i].link != 0)
					out << "\t\t\tnextpage = " << static_cast<int> (abs(static_cast<double> (speech.responses[i].link))) << ";\n\t\t\tclosedialog = " << (speech.responses[i].link >= 0 ? "true" : "false" ) << ";\n";
				out << "\t\t}\n";
			}
		}
		out << "\t}\n";
	}
	if(lastID != -1)
		out << "}\n";
}
