// Copyright (c) 2010, Braden "Blzut3" Obrzut <admin@maniacsvault.net>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
#include <cstdarg>
#include <string>

#include "compile.h"

using namespace std;

enum Mode
{
	MODE_COMPILE,
	MODE_COMPILEOLD,
	MODE_DECOMPILE,
	MODE_DECOMPILEOLD
};

void Error(const char* message, ...)
{
#ifdef unix
	cerr << "\033[1m\033[31mError: ";
#else
	cerr << "Error: ";
#endif

	va_list list;
	va_start(list, message);
	vfprintf(stderr, message, list);
	va_end(list);

#ifdef unix
	cerr << "\033[0m\n";
#else
	cerr << "\n";
#endif
	exit(1);
}

void Warning(const char* message, ...)
{
#ifdef unix
	cerr << "\033[1m\033[33mWarning: ";
#else
	cerr << "Warning: ";
#endif

	va_list list;
	va_start(list, message);
	vfprintf(stderr, message, list);
	va_end(list);

#ifdef unix
	cerr << "\033[0m\n";
#else
	cerr << "\n";
#endif
}

const char* GenerateOutfileFromInfile(const string infile)
{
	// Look for an extension and replace with or add .o
	char* ret = new char[infile.length()+3];
	sprintf(ret, "%s.o", infile.substr(0, infile.find_last_of('.')).c_str());
	return ret;
}

int main(int argc, char* argv[])
{
	if(argc <= 1)
	{
		cout << "Universal Strife Dialog Compiler                                    Version 1.7\n"
		        "Copyright 2010-2019 Braden \"Blzut3\" Obrzut\n"
		        "_______________________________________________________________________________\n\n"
		        "Usage: usdc [options] infile [-o outfile]\n"
		        "    -c        Compile the input file. (DEFAULT)\n"
		        "    -t        Compile in old teaser format, not recommend.\n"
		        "    -d        Decompile the input file.\n"
		        "    -o        Output file (use \"-\" for stdout).\n"
		        "    -k        Keep cost item numbers even if zero (decompile).\n"
		        "    -r        Remove blank yes messages like ZDoom (decompile).\n";
	}
	else
	{
		bool removeblanks = false;
		bool keepcostitems = false;
		Mode mode = MODE_COMPILE;
		const char* outfile = NULL;
		const char* infile = NULL;
		for(int i = 1;i < argc;i++)
		{
			if(argv[i][0] == '-' && strlen(argv[i]) == 2)
			{
				switch(argv[i][1])
				{
					case 'c':
						mode = MODE_COMPILE;
						break;
					case 't':
						mode = mode == MODE_DECOMPILE ? MODE_DECOMPILEOLD : MODE_COMPILEOLD;
						break;
					case 'd':
						mode = mode == MODE_COMPILEOLD ? MODE_DECOMPILEOLD : MODE_DECOMPILE;
						break;
					case 'o':
						if(i+1 < argc)
						{
							i++;
							outfile = argv[i];
						}
						break;
					case 'k':
						keepcostitems = true;
						break;
					case 'r':
						removeblanks = true;
						break;
					default:
						break;
				}
			}
			else
				infile = argv[i];
		}
		if(infile == NULL)
			Error("No input file!");
		if(outfile == NULL)
			outfile = GenerateOutfileFromInfile(infile);

		// Read in the file
		fstream input(infile, ios_base::in|ios_base::binary);
		if(!input.is_open())
			Error("Could not open input file!");
		input.seekg(0, ios_base::end);
		int size = input.tellg();
		char *buffer = new char[size];
		memset(buffer, 0, size);
		input.seekg(0, ios_base::beg);
		input.read(buffer, size);
		if(input.fail() && !input.eof())
		{
			delete[] buffer;
			Error("Could not read input file!");
		}
		input.close();

		fstream outfileStream;
		ostream *output = &cout;
		if(strcmp(outfile, "-") != 0)
		{
			outfileStream.open(outfile, ios_base::out | ios_base::binary | ios_base::trunc);
			if(!outfileStream.is_open())
			{
				delete[] buffer;
				Error("Could not open the output file for writing.");
			}
			else
				output = &outfileStream;
		}

		switch(mode)
		{
			default:
				Compile(buffer, size, *output, mode == MODE_COMPILEOLD);
				break;
			case MODE_DECOMPILE:
			case MODE_DECOMPILEOLD:
				Decompile(buffer, size, *output, mode == MODE_DECOMPILEOLD, removeblanks, keepcostitems);
				break;
		}

		delete[] buffer;
	}
	return 0;
}
