find_program(SPHINX_BUILD_EXECUTABLE
	NAMES sphinx-build sphinx-build-3
	DOC "Sphinx documentation builder (sphinx-doc.org)"
)

if(SPHINX_BUILD_EXECUTABLE)
	add_executable(sphinx::build IMPORTED)
	set_target_properties(sphinx::build PROPERTIES IMPORTED_LOCATION ${SPHINX_BUILD_EXECUTABLE})
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Sphinx
	REQUIRED_VARS SPHINX_BUILD_EXECUTABLE
)
